# Changelog

## [2.0.0] - 2024-08-10 

### Added
 - castle_gates (Reason: has been part of castles mod in an earlier version)
 - libraries: fmod, futil (Reason: solve smartshop dependencies)
 - this changelog
### Changed
 - updated mtg_core to latest mtg game
#### Submodule repository URLs
 - basic_materials (Reason: invalid URL)
 - mobs_redo (Reason: invalid URL)
 - mobs_animal (Reason: invalid URL)
 - mob_horse (Reason: invalid URL)
 - pipeworks (Reason: invalid URL)
 - unifieddyes (Reason: invalid URL)
 - 3dforniture (Reason: invalid URL)
 - arrow_signs (Reason: invalid URL)
 - db (Reason: invalid URL)
 - 3d_armor (Reason: abandoned)
 - beer_test (Reason: abandoned)
 - bobblocks (Reason: abandoned, now maintained by KA)
 - smartshop
      - previously not a submodule
      - extremely outdated version
      - replaced by flux's fork
#### Submodule updates
Almost all submodules has been updated to most recent commit.

**Sokomine's cottages**

Only Sokomines cottages mod has been set to commit `8a2c391`, since it fixes all deprecation notices.
Later commits introduced nodes with undocumented dependencies to ethereal mod for having just 2 graphics.
Missing dependency is not catched and Sokomines repository seems to be abandoned now.

There's an actively developed [fork](https://github.com/C-C-Minetest-Server/cottages) which fixes a lot of cottages' issues but it also removed some straw roof nodes.
It's possible to replace them with same looking nodes that are part of king arthurs game but that would lead to high map maintainance effort.

So replacing cottages is postponed to a later update (needs migration!)
### Fixed
 - game.conf's `name` property is now `title`
 - added `mod.conf` and removed empty `depends.txt` from helloscreen
 - fixed candles mod's deprecated calls
 - fixed king-arthurs-land's dirt water's depreacted properties