minetest.register_node("king_arthurs_land:dirt_water_source", {
	description = "Dirty Water Source",
	drawtype = "liquid",
	tiles = {
		{
			name = "king_arthurs_land_dirt_water_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	special_tiles = {
		-- New-style water source material (mostly unused)
		{
			name = "king_arthurs_land_dirt_water_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
			backface_culling = false,
		},
	},
	use_texture_alpha="clip",
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "king_arthurs_land:dirt_water_flowing",
	liquid_alternative_source = "king_arthurs_land:dirt_water_source",
	liquid_viscosity = 7,
	post_effect_color = {a = 160, r = 137, g = 96, b = 63},
	groups = {water = 3, liquid = 3, puts_out_fire = 1},
})

minetest.register_node("king_arthurs_land:dirt_water_flowing", {
	description = "Dirty Flowing Water",
	drawtype = "flowingliquid",
	tiles = {"king_arthurs_land_dirt_water.png"},
	special_tiles = {
		{
			name = "king_arthurs_land_dirt_water_flowing_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
		{
			name = "king_arthurs_land_dirt_water_flowing_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
	},
	use_texture_alpha="clip",
	paramtype = "light",
	paramtype2 = "flowingliquid",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_alternative_flowing = "king_arthurs_land:dirt_water_flowing",
	liquid_alternative_source = "king_arthurs_land:dirt_water_source",
	liquid_viscosity = 7,
	post_effect_color = {a = 160, r = 137, g = 96, b = 63},
	groups = {water = 3, liquid = 3, puts_out_fire = 1,
		not_in_creative_inventory = 1},
})


-- bucket stuff
minetest.register_alias("bucket_dirt_water", "bucket:bucket_dirt_water")

bucket.register_liquid(
	"king_arthurs_land:dirt_water_source",
	"king_arthurs_land:dirt_water_flowing",
	"king_arthurs_land:bucket_dirt_water",
	"bucket_dirt_water.png",
	"Dirty Water Bucket",
	{water_bucket = 1}
)





