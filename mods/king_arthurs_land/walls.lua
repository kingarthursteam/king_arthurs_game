local MODNAME = "king_arthurs_land"
walls.register(MODNAME ..":stonebrick", "Stonebrick Wall", "default_stone_brick.png",
		"default:stonebrick", default.node_sound_stone_defaults())
		
walls.register(MODNAME ..":sandstone", "Sandstone Wall", "default_sandstone.png",
		"default:sandstone", default.node_sound_stone_defaults())

walls.register(MODNAME ..":sandstone_brick", "Sandstone Brick Wall", "default_sandstone_brick.png",
		"default:sandstone_brick", default.node_sound_stone_defaults())
		
walls.register(MODNAME ..":tree", "Palisade", "default_tree.png",
		"default:tree", default.node_sound_wood_defaults())
