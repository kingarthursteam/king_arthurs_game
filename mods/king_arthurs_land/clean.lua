-- Clean mod 2.1
--
-- (c) 2016 King Arthurs Team

local old_nodes = {}

local MODNAME = minetest.get_current_modname()
local MODPATH = minetest.get_modpath(MODNAME)
local file = MODPATH..DIR_DELIM.."unknown-nodes.txt"
for node_name in io.lines(file) do
	table.insert(old_nodes, node_name:trim())
end

minetest.register_lbm({
        name = MODNAME..":old_nodes",
        nodenames = old_nodes,
    --  ^ List of node names to trigger the LBM on.
    --    Also non-registered nodes will work.
    --    Groups (as of group:groupname) will work as well.
        run_at_every_load = true,
    --  ^ Whether to run the LBM's action every time a block gets loaded,
    --    and not just for blocks that were saved last time before LBMs were
    --    introduced to the world.
        action = minetest.remove_node
    })
