--[[
	add compatibility with the old nodes of king arthurs land
	
	minetest.register_alias("name", "convert_to")
	
	name is converted to convert_to by the engine. This means that if the engine finds nodes with the name name in the world the node with the name convert_to is used instead.
	This is useful to maintain backwards compatibility.
	There are also other cases like map generation where aliases are used. The /giveme command also looks for aliases when adding the item to the players inventory. 
]]

-- Added at 10.07.2016
minetest.register_alias("moreores:mineral_copper", "default:stone_with_copper")
minetest.register_alias("moreores:mineral_gold", "default:stone_with_gold")

-- Added at 11.09.2016
minetest.register_alias("titanium:titanium_in_ground", "default:diamondblock")
