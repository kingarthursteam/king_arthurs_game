-- ***********************************************************************************
--													   ***********************************************
--					4SEASONS							**************************************************
--													   ***********************************************
-- ***********************************************************************************
minetest.register_alias("4seasons:slimtree","king_arthurs_land:slimtree")
minetest.register_alias("king_arthurs_land:slimtree_wood","dark_fence")
minetest.register_alias("4seasons:palmtree","king_arthurs_land:palmtree")
minetest.register_alias("4seasons:palmleaves","king_arthurs_land:palmleaves")
minetest.register_alias("4seasons:slimtree_wood","king_arthurs_land:slimtree_wood")
PLANTLIKE = function(nodeid, nodename,type,option)
	if option == nil then option = false end

	local params ={ description = nodename, drawtype = "plantlike", tiles = {"4seasons_"..nodeid..'.png'}, 
	inventory_image = "4seasons_"..nodeid..'.png',	wield_image = "4seasons_"..nodeid..'.png', paramtype = "light",	}
		
	if type == 'veg' then
		params.groups = {snappy=2,dig_immediate=3,flammable=2}
		params.sounds = default.node_sound_leaves_defaults()
		if option == false then params.walkable = false end
	elseif type == 'met' then			-- metallic
		params.groups = {cracky=3}
		params.sounds = default.node_sound_stone_defaults()
	elseif type == 'cri' then			-- craft items
		params.groups = {snappy=2,choppy=2,oddly_breakable_by_hand=2,flammable=3}
		params.sounds = default.node_sound_wood_defaults()
		if option == false then params.walkable = false end
	elseif type == 'eat' then			-- edible
		params.groups = {snappy=2,choppy=2,oddly_breakable_by_hand=2,flammable=3}
		params.sounds = default.node_sound_wood_defaults()
		params.walkable = false
		params.on_use = minetest.item_eat(option)
	end
	minetest.register_node("king_arthurs_land:"..nodeid, params)
end



-- ***********************************************************************************
--		SLIMTREES							**************************************************
-- ***********************************************************************************
minetest.register_abm({
		nodenames = { "king_arthurs_land:slimtree" },
		interval = 120,
		chance = 1,
		
		action = function(pos, node, active_object_count, active_object_count_wider)
			minetest.env:add_node({x=pos.x,y=pos.y,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})
			minetest.env:add_node({x=pos.x,y=pos.y+1,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})
			minetest.env:add_node({x=pos.x,y=pos.y+2,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})

			minetest.env:add_node({x=pos.x,y=pos.y+3,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})			
			minetest.env:add_node({x=pos.x+1,y=pos.y+3,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x-1,y=pos.y+3,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+3,z=pos.z+1},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+3,z=pos.z-1},{type="node",name="default:leaves"})			


			minetest.env:add_node({x=pos.x,y=pos.y+4,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x+1,y=pos.y+4,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x-1,y=pos.y+4,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+4,z=pos.z+1},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+4,z=pos.z-1},{type="node",name="default:leaves"})			


			minetest.env:add_node({x=pos.x,y=pos.y+5,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x+1,y=pos.y+5,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x-1,y=pos.y+5,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+5,z=pos.z+1},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+5,z=pos.z-1},{type="node",name="default:leaves"})			

			minetest.env:add_node({x=pos.x,y=pos.y+6,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x+1,y=pos.y+6,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x-1,y=pos.y+6,z=pos.z},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+6,z=pos.z+1},{type="node",name="default:leaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+6,z=pos.z-1},{type="node",name="default:leaves"})			
		end
})
-- ***********************************************************************************
--		PALM TREES							**************************************************
-- ***********************************************************************************
minetest.register_abm({
		nodenames = { "king_arthurs_land:palmtree" },
		interval = 120,
		chance = 1,
	
		action = function(pos, node, active_object_count, active_object_count_wider)
			minetest.env:add_node({x=pos.x,y=pos.y,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})
			minetest.env:add_node({x=pos.x,y=pos.y+1,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})
			minetest.env:add_node({x=pos.x,y=pos.y+2,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})
			minetest.env:add_node({x=pos.x,y=pos.y+3,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})
			minetest.env:add_node({x=pos.x,y=pos.y+4,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})						
			minetest.env:add_node({x=pos.x,y=pos.y+5,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})			
			minetest.env:add_node({x=pos.x,y=pos.y+6,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})												
			minetest.env:add_node({x=pos.x,y=pos.y+7,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})						
			minetest.env:add_node({x=pos.x,y=pos.y+8,z=pos.z},{type="node",name="king_arthurs_land:slimtree_wood"})						
			minetest.env:add_node({x=pos.x+2,y=pos.y+8,z=pos.z},{type="node",name="king_arthurs_land:palmleaves"})			
			minetest.env:add_node({x=pos.x-2,y=pos.y+8,z=pos.z},{type="node",name="king_arthurs_land:palmleaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+8,z=pos.z+2},{type="node",name="king_arthurs_land:palmleaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+8,z=pos.z-2},{type="node",name="king_arthurs_land:palmleaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+9,z=pos.z},{type="node",name="king_arthurs_land:palmleaves"})			
			minetest.env:add_node({x=pos.x+1,y=pos.y+9,z=pos.z},{type="node",name="king_arthurs_land:palmleaves"})			
			minetest.env:add_node({x=pos.x-1,y=pos.y+9,z=pos.z},{type="node",name="king_arthurs_land:palmleaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+9,z=pos.z+1},{type="node",name="king_arthurs_land:palmleaves"})			
			minetest.env:add_node({x=pos.x,y=pos.y+9,z=pos.z-1},{type="node",name="king_arthurs_land:palmleaves"})			
		end
})

-- ***********************************************************************************
--		NODES									**************************************************
-- ***********************************************************************************

PLANTLIKE('palmtree','Palmtree Sapling','veg')
PLANTLIKE('slimtree','Slimtree Sapling','veg')


minetest.register_node("king_arthurs_land:palmleaves", {
	description = "Palmleaves",
	drawtype = "raillike",
	tiles = {"4seasons_palmleaves.png", "4seasons_palmleaves_top.png", "4seasons_palmleaves_top.png", "4seasons_palmleaves_top.png"},
	inventory_image = "4seasons_palmleaves.png",
	wield_image = "4seasons_palmleaves.png",
	paramtype = "light",
	is_ground_content = true,
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, -1/2+1/16, 1/2},
	},
	groups = {snappy=1,dig_immediate=2},
})
minetest.register_node("king_arthurs_land:slimtree_wood", {
	description = "Slimtree",
	drawtype = "fencelike",
	tiles = {"default_tree.png"},
--	inventory_image = "default_tree.png",
--	wield_image = "default_tree.png",
	paramtype = "light",
	is_ground_content = true,
	selection_box = {
		type = "fixed",
		fixed = {-1/7, -1/2, -1/7, 1/7, 1/2, 1/7},
	},
	groups = {tree=1,snappy=2,choppy=2,oddly_breakable_by_hand=2,flammable=2},
	sounds = default.node_sound_wood_defaults(),
	--drop = 'default:fence_wood',
})

minetest.register_craft({	output = 'king_arthurs_land:slimtree 1',	recipe = {
		{'','default:leaves',''},
		{'','default:leaves',''},
		{'','default:stick',''},
	}})
minetest.register_craft({	output = 'king_arthurs_land:palmtree 1',	recipe = {
		{'default:leaves','default:stick','default:leaves'},
		{'','default:stick',''},
		{'','default:stick',''},
	}})
