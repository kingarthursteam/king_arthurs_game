--[[
	King-Arthurs-land Mod
	=====================
	
	(c) 2016-2020 King Arthurs Team

]]

local MODPATH = minetest.get_modpath("king_arthurs_land")

dofile(MODPATH.."/clean.lua");
dofile(MODPATH.."/aliases.lua");
dofile(MODPATH.."/dirt_water.lua");
dofile(MODPATH.."/iron_xdoors2.lua");
dofile(MODPATH.."/trees.lua");
dofile(MODPATH.."/walls.lua")

--does not work (currently due a bug of minetest)
minetest.override_item("default:lava_source", {
    groups = {lava = 3, liquid = 2, hot = 3, igniter = nil},
})
minetest.override_item("default:lava_flowing", {
    	groups = {lava = 3, liquid = 2, hot = 3, igniter = nil,
		not_in_creative_inventory = 1},
})

local list = {}
local converter = {}
ra = function(old,new)
	table.insert(list, old)
	converter[old]=new
end
local function register_moreblocks_alias(origin, new)
    local originmod = origin:split(":")[1]
    local originnode = origin:split(":")[2]
    local newmod = new:split(":")[1]
    local newnode = new:split(":")[2]
    
	ra(originmod.. ":micro_" ..originnode.."_1",                      newmod..":micro_" ..newnode.."_1")	
	ra(originmod.. ":panel_" ..originnode.."_1",                      newmod..":panel_" ..newnode.."_1")
	ra(originmod.. ":micro_" ..originnode.."_2",                      newmod..":micro_" ..newnode.."_2")
	ra(originmod.. ":panel_" ..originnode.."_2",                      newmod..":panel_" ..newnode.."_2")
	ra(originmod.. ":micro_" ..originnode.."_4",                      newmod..":micro_" ..newnode.."_4")
	ra(originmod.. ":panel_" ..originnode.."_4",                      newmod..":panel_" ..newnode.."_4")
	ra(originmod.. ":micro_" ..originnode,                            newmod..":micro_" ..newnode)
	ra(originmod.. ":panel_" ..originnode,                            newmod..":panel_" ..newnode)
	ra(originmod.. ":micro_" ..originnode.."_12",                     newmod..":micro_" ..newnode.."_12")
	ra(originmod.. ":panel_" ..originnode.."_12",                     newmod..":panel_" ..newnode.."_12")
	ra(originmod.. ":micro_" ..originnode.."_14",                     newmod..":micro_" ..newnode.."_14")
	ra(originmod.. ":panel_" ..originnode.."_14",                     newmod..":panel_" ..newnode.."_14")
	ra(originmod.. ":micro_" ..originnode.."_15",                     newmod..":micro_" ..newnode.."_15")
	ra(originmod.. ":panel_" ..originnode.."_15",                     newmod..":panel_" ..newnode.."_15")
	
	ra(originmod.. ":stair_" ..originnode.."_outer",                  newmod..":stair_" ..newnode.."_outer")
	ra(originmod.. ":stair_" ..originnode,                            newmod..":stair_" ..newnode)
	ra(originmod.. ":stair_" ..originnode.."_inner",                  newmod..":stair_" ..newnode.."_inner")
	
	ra(originmod.. ":slab_" ..originnode.."_1",                       newmod..":slab_" ..newnode.."_1")
	ra(originmod.. ":slab_" ..originnode.."_2",                       newmod..":slab_" ..newnode.."_2")
	ra(originmod.. ":slab_" ..originnode.."_quarter",                 newmod..":slab_" ..newnode.."_quarter")
	ra(originmod.. ":slab_" ..originnode,                             newmod..":slab_" ..newnode)
	ra(originmod.. ":slab_" ..originnode.."_three_quarter",           newmod..":slab_" ..newnode.."_three_quarter")
	ra(originmod.. ":slab_" ..originnode.."_14",                      newmod..":slab_" ..newnode.."_14")
	ra(originmod.. ":slab_" ..originnode.."_15",                      newmod..":slab_" ..newnode.."_15")
	
	ra(originmod.. ":stair_" ..originnode.."_half",                   newmod..":stair_" ..newnode.."_half")
	
	ra(originmod.. ":stair_" ..originnode.."_alt_1",                  newmod..":stair_" ..newnode.."_alt_1")
	ra(originmod.. ":stair_" ..originnode.."_alt_2",                  newmod..":stair_" ..newnode.."_alt_2")
	ra(originmod.. ":stair_" ..originnode.."_alt_4",                  newmod..":stair_" ..newnode.."_alt_4")
	ra(originmod.. ":stair_" ..originnode.."_alt",                    newmod..":stair_" ..newnode.."_alt")
	ra(originmod.. ":slope_" ..originnode,                            newmod..":slope_" ..newnode)
	ra(originmod.. ":slope_" ..originnode.."_half",                   newmod..":slope_" ..newnode.."_half")
	ra(originmod.. ":slope_" ..originnode.."_half_raised",            newmod..":slope_" ..newnode.."_half_raised")
	
	ra(originmod.. ":slope_" ..originnode.."_inner",                  newmod..":slope_" ..newnode.."_inner")
	ra(originmod.. ":slope_" ..originnode.."_inner_half",             newmod..":slope_" ..newnode.."_inner_half")
	ra(originmod.. ":slope_" ..originnode.."_inner_half_raised",      newmod..":slope_" ..newnode.."_inner_half_raised")
	ra(originmod.. ":slope_" ..originnode.."_inner_cut",              newmod..":slope_" ..newnode.."_inner_cut")
	ra(originmod.. ":slope_" ..originnode.."_inner_cut_half",         newmod..":slope_" ..newnode.."_inner_cut_half")
	ra(originmod.. ":slope_" ..originnode.."_inner_cut_half_raised",  newmod..":slope_" ..newnode.."_inner_cut_half_raised")
	
	ra(originmod.. ":slope_" ..originnode.."_outer",                  newmod..":slope_" ..newnode.."_outer")
	ra(originmod.. ":slope_" ..originnode.."_outer_half",             newmod..":slope_" ..newnode.."_outer_half")
	ra(originmod.. ":slope_" ..originnode.."_outer_half_raised",      newmod..":slope_" ..newnode.."_outer_half_raised")
	ra(originmod.. ":slope_" ..originnode.."_outer_cut",              newmod..":slope_" ..newnode.."_outer_cut")
	ra(originmod.. ":slope_" ..originnode.."_outer_cut_half",         newmod..":slope_" ..newnode.."_outer_cut_half")
	ra(originmod.. ":slope_" ..originnode.."_outer_cut_half_raised",  newmod..":slope_" ..newnode.."_outer_cut_half_raised")
	ra(originmod.. ":slope_" ..originnode.."_cut",                    newmod..":slope_" ..newnode.."_cut")
end
register_moreblocks_alias("moreblocks:cobble", "darkage:chalked_bricks")
register_moreblocks_alias("default:stonebrick", "moreblocks:coal_stone_bricks")
ra("default:cobble", "darkage:chalked_bricks")
ra("default:stonebrick", "moreblocks:coal_stone_bricks")
ra("king_arthurs_land:hardwood", "default:junglewood")
ra("building_blocks:hardwood", "default:junglewood")
ra("king_arthurs_land:stair_hardwood", "stairs:stair_junglewood")
ra("building_blocks:stair_hardwood", "stairs:stair_junglewood")
ra("king_arthurs_land:slab_hardwood", "stairs:slab_junglewood")
ra("building_blocks:slab_hardwood", "stairs:slab_junglewood")

minetest.register_lbm({
	name="king_arthurs_land:transform_2",--increase this number after modifying any ra operation
	nodenames= list,
	run_at_every_load = false,
	action = function(pos,node)
		if node.name and converter[node.name] then
			node.name = converter[node.name]
			minetest.swap_node(pos, node)
		end
	end
})
