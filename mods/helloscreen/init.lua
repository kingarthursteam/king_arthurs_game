--[[
	Helloscreen mod by addi

	(c) 2017 King-Arthurs-Team

	Version 1.2
]]

local text = [[Hi playername,
welcome on King Arthurs Land!

You can walk around and use the Teleporters 
to explore this server. If you want to build 
here please register at www.king-arthur.eu

Your friend Merlin]]

local helloscreen_players = {}
local function check_helloscreen(player)

	local playername = player:get_player_name();
	if not minetest.check_player_privs(playername, {interact=true}) then
		helloscreen_players[playername] = {0,0}
		helloscreen_players[playername][1] = player:hud_add({
			hud_elem_type = "image",
			scale = {x=1,y=0.6},
			size = "",
			text = "schriftrolle.png",
			position = {x=1,y=0},--top right
			number = 20,
			alignment = {x=-1,y=1},
			offset = {x=0, y=0},
		})
		helloscreen_players[playername][2] = player:hud_add({
			text = string.gsub(text,"playername",playername),
			hud_elem_type = "text",
			position = {x=1,y=0},--top right
			number = 0xFFFFFF,--number is color (stupid api)
			alignment = {x=-1,y=1},
			offset = {x=-22, y=20},
		})
	else 
		if(helloscreen_players[playername]) then
			player:hud_remove(helloscreen_players[playername][1])
			player:hud_remove(helloscreen_players[playername][2])
			helloscreen_players[playername]=nil
		end
	end
end
minetest.register_on_joinplayer(check_helloscreen);

local real_minetest_notify_authentication_modified = minetest.notify_authentication_modified;
minetest.notify_authentication_modified = function(playername)
	check_helloscreen(minetest.get_player_by_name(playername));
	real_minetest_notify_authentication_modified(playername)
end
