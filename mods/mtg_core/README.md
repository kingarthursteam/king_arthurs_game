This Modpack contains some core mods from mintest_game

#Update#

due a bug of minetest https://forum.minetest.net/viewtopic.php?f=6&t=13870 you have to modify default/nodes.lua manualy after each update!

Please make shure there are no esential texture updates in minetest_game. If there are some, you have to place the old textures in ../king_arthurs_land/ 
and override it like it was done in ../king_arthurs_land/init.lua

Dont add more mods of Minetest game to this folder except of this:
* beds
* boats
* bucket
* creative
* default
* doors
* dye
* fire
* give_initial_stuff
* screwdriver
* sethome
* stairs
* wool
* xpanes